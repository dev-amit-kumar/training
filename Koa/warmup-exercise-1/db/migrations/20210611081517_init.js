exports.up = function (knex) {
	return knex.schema.createTable('products', (table) => {
		table.increments('id');
		table.string('name').notNullable();
		table.integer('price');
		table.integer('quantity');
		table.timestamps(true, true);
	});
};

exports.down = function (knex) {
	return knex.schema.dropTable('products');
};
