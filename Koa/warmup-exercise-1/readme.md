## To make new migration table

npx knex migrate:make init --migrations-directory db/migrations

## To apply migrations

npx knex migrate:latest --knexfile db/knexfile.js
