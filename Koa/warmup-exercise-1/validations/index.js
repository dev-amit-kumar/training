const { loginSchema, registerSchema } = require('./schema');

exports.loginValidation = async (ctx, next) => {
	const { error } = loginSchema.validate(ctx.request.body);
	if (error) {
		return (ctx.body = {
			message: 'Login failed',
			error: error.details[0].message,
		});
	}
	await next();
};

exports.registerValidation = async (ctx, next) => {
	var { error } = registerSchema.validate(ctx.request.body);
	if (error) {
		return (ctx.body = {
			message: 'Register failed',
			error: error.details[0].message,
		});
	}
	await next();
};
