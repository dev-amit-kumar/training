const db = require('../db');

exports.findEmail = async (email) => {
	try {
		const userData = await db('user')
			.select('id', 'name', 'email', 'password')
			.where({ email });
		return userData[0];
	} catch (error) {
		throw error;
	}
};

exports.registerUser = async (data) => {
	try {
		const [id] = await db('user').insert(data).returning('id');
		return id;
	} catch (error) {
		throw error;
	}
};

exports.getUserById = async (id) => {
	try {
		const data = await db('user')
			.select('id', 'name', 'email')
			.where({ id });
		return data;
	} catch (error) {
		throw error;
	}
};
