const db = require('../db');

exports.getProductsModel = async () => {
	try {
		const allProducts = await db('products').select();
		return allProducts;
	} catch (error) {
		throw error;
	}
};

exports.updateProductModel = async (id, data) => {
	try {
		const { name, price, quantity } = data;
		const isUpdated = await db('products')
			.where({ id: id })
			.update({ name, price, quantity });
		return isUpdated;
	} catch (error) {
		throw error;
	}
};
