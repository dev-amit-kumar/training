const Koa = require('koa');
const bodyParser = require('koa-bodyparser');

const port = 3000;

const app = new Koa();
const router = require('./routes');

app.use(bodyParser());
app.use(router.routes()).use(router.allowedMethods);
// default error handler
app.on('error', (err) => {
	console.error('server error', err);
});

app.listen(port, () => console.log(`Server running at ${port}`));
