const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { getUserById, findEmail, registerUser } = require('../model/user');
const { jwt_secret } = require('../utils/config');

exports.loginUser = async (ctx) => {
	try {
		const { email, password } = ctx.request.body;
		// checking email exist
		const userData = await findEmail(email);
		if (!userData) {
			return (ctx.body = { message: 'Email id doesnot exist' });
		}

		// checking password is correct or not
		const checkPassword = bcrypt.compareSync(password, userData.password);
		if (!checkPassword) {
			return (ctx.body = { message: 'Invalid password' });
		}

		// generating jwt token
		const token = jwt.sign({ id: userData.id }, jwt_secret, {
			expiresIn: 3600,
		});
		return (ctx.body = { message: 'Login succesfull', token });
	} catch (error) {
		console.log(error);
		ctx.body = error;
		ctx.response.status = 500;
	}
};

exports.registerUser = async (ctx) => {
	try {
		const { email, password, name } = ctx.request.body;
		// checking email exist
		const isUser = await findEmail(email);
		if (isUser) {
			return (ctx.body = { message: 'Email id already exist' });
		}
		// making new user data to insert in db
		const newUserData = {
			email,
			name,
			password: bcrypt.hashSync(password, 8),
		};
		await registerUser(newUserData);
		return (ctx.body = { message: 'Register successfull' });
	} catch (error) {
		ctx.body = error;
		ctx.response.status = 500;
	}
};

exports.authChecker = async (ctx, next) => {
	authToken = ctx.request.headers['auth-token'];
	if (!authToken) return (ctx.body = { message: 'Login to continue' });
	try {
		const userId = jwt.verify(authToken, jwt_secret);
		userObj = await getUserById(userId.id);

		// if user id doesnot exist
		if (!userObj.length) {
			return (ctx.body = { message: 'Login failed' });
		}
		// making object of user data
		ctx.request.userObj = userObj;

		await next();
	} catch (error) {
		ctx.body = { message: 'Login expired' };
	}
};
