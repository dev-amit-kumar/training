const { getProductsModel, updateProductModel } = require('../model/products');

exports.getProducts = async (ctx) => {
	try {
		const allProduct = await getProductsModel();
		if (!allProduct.length) {
			return (ctx.body = { message: 'No Products found' });
		}
		return (ctx.body = { message: 'All products', data: allProduct });
	} catch (error) {
		ctx.body = error;
		ctx.response.status = 500;
	}
};

exports.updateProduct = async (ctx) => {
	try {
		const { id: productId } = ctx.params;
		updatedData = await updateProductModel(productId, ctx.request.body);
		if (updatedData) {
			return (ctx.body = {
				message: 'Product detail updated',
			});
		}
		return (ctx.body = { message: 'Product not found' });
	} catch (error) {
		ctx.body = error;
		ctx.response.status = 500;
	}
};
