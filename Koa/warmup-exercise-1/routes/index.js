const KoaRouter = require('koa-router');
const router = new KoaRouter();

const { loginValidation, registerValidation } = require('../validations/index');
const { loginUser, registerUser, authChecker } = require('../controller/auth');
const { getProducts, updateProduct } = require('../controller/products');

router.post('/login', loginValidation, loginUser);
router.post('/register', registerValidation, registerUser);
router.get('/getProducts', authChecker, getProducts);
router.put('/updateProduct/:id', authChecker, updateProduct);

module.exports = router;
