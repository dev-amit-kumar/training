import React from 'react';
import Detail from './index';
import { mount } from 'enzyme';

describe('Testing props', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = mount(<Detail />);
	});

	test('Render description', () => {
		expect(wrapper.find('.desc').text()).toBe('The description goes here');
	});

	test('Render title', () => {
		expect(wrapper.find('.title').text()).toBe('A Title');
	});
});
