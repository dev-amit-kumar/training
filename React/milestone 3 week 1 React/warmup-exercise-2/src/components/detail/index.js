import React from 'react';
import './detail.css';

const default_prop = {
	description: 'The description goes here',
	heading: 'A Title',
};

const Detail = ({ description, heading }) => {
	return (
		<div>
			<h2 className="title">{heading}</h2>
			<p className="desc">{description}</p>
		</div>
	);
};

Detail.defaultProps = default_prop;

export default Detail;
