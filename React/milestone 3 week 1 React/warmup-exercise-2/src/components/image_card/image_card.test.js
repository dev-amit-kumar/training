import React from 'react';
import Image_card from './index';
import { mount } from 'enzyme';

describe('Testing props', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = mount(<Image_card />);
	});

	test('Render image inside the outer rectangle div', () => {
		expect(wrapper.find('img').prop('src')).toEqual(
			'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0PR2ZAzwhWY7orX3aNxJE67X5TaAjAN7H_g&usqp=CAU',
		);
	});
});
