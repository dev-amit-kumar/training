import Detail from '../detail';
import './image_card.css';

const ImageCard = () => {
	return (
		<div className="outer-rectangle">
			<img
				alt="img rectangle"
				src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0PR2ZAzwhWY7orX3aNxJE67X5TaAjAN7H_g&usqp=CAU"
			/>
			<Detail />
		</div>
	);
};

export default ImageCard;
