import React from 'react';
import ReactDOM from 'react-dom';
import ImageCard from './components/image_card';

ReactDOM.render(
	<React.StrictMode>
		<ImageCard />
	</React.StrictMode>,
	document.getElementById('root'),
);
