import React from 'react';
import ReactDOM from 'react-dom';
import Calculator from './components/calculator';

ReactDOM.render(
	<React.StrictMode>
		<Calculator />
	</React.StrictMode>,
	document.getElementById('root'),
);
