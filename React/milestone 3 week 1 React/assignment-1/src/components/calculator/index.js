import React, { useState } from 'react';
import Button from '../button/';
import './calculator.scss';

const Calculator = () => {
	const [output, setOutput] = useState('0');
	const [isEmpty, setEmpty] = useState(true);

	const calculate = () => {
		try {
			setOutput(eval(output).toString());
		} catch (error) {
			setOutput('Error');
		}
	};

	const clear = () => {
		setOutput('0');
		setEmpty(true);
	};

	const handleClick = ({ name }) => {
		if (isEmpty) {
			setOutput(name);
			setEmpty(false);
		} else {
			setOutput(output.concat(name));
		}
	};

	return (
		<div className="calculator">
			<p className="output">{output}</p>
			<Button cls="clear_btn" fn={() => clear()} val="clear" />
			<Button cls="operation" fn={(e) => handleClick(e.target)} val="/" />
			<Button fn={(e) => handleClick(e.target)} val="7" />
			<Button fn={(e) => handleClick(e.target)} val="8" />
			<Button fn={(e) => handleClick(e.target)} val="9" />
			<Button cls="operation" fn={(e) => handleClick(e.target)} val="-" />
			<Button fn={(e) => handleClick(e.target)} val="4" />
			<Button fn={(e) => handleClick(e.target)} val="5" />
			<Button fn={(e) => handleClick(e.target)} val="6" />
			<Button cls="operation" fn={(e) => handleClick(e.target)} val="+" />
			<Button fn={(e) => handleClick(e.target)} val="1" />
			<Button fn={(e) => handleClick(e.target)} val="2" />
			<Button fn={(e) => handleClick(e.target)} val="3" />
			<Button cls="operation" fn={() => calculate()} val="=" />
		</div>
	);
};

export default Calculator;
