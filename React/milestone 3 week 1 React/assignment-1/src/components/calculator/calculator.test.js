import Calculator from './index';
import { mount } from 'enzyme';

describe('Calculator testing', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = mount(<Calculator />);
	});

	test('Render the initial output of the calculator', () => {
		expect(wrapper.find('.output').text()).toBe('0');
	});
});
