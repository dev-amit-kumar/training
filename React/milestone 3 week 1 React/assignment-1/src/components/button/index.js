import React from 'react';
import { func, string } from 'prop-types';
import './button.scss';

const prop_types = {
	// cls is the classname of the button
	cls: string,
	// fn is the callback function of the button
	fn: func.isRequired,
	// val is the value of the button
	val: string.isRequired,
};

const Button = ({ cls, fn, val }) => {
	return (
		<button className={cls} name={val} onClick={fn}>
			{val === '/' ? <span>&#247;</span> : val}
		</button>
	);
};

Button.propTypes = prop_types;

export default Button;
