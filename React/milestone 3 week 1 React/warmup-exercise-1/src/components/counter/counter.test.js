import Counter from './index';
import { shallow } from 'enzyme';

describe('Counter Testing', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = shallow(<Counter />);
	});

	test('Render the initial value of state in a div', () => {
		expect(wrapper.find('#counter-value').text()).toBe('0');
	});

	// Previous written test cases

	// test('Render increment button', () => {
	// 	expect(wrapper.find('#increment-btn').text()).toBe('+');
	// });

	// test('Render decrement button', () => {
	// 	expect(wrapper.find('#decrement-btn').text()).toBe('-');
	// });

	// test('Render the click event of increment button and counter value', () => {
	// 	wrapper.find('#increment-btn').simulate('click');
	// 	expect(wrapper.find('#counter-value').text()).toBe('1');
	// });

	// test('Render the click event of decrement button and counter value', () => {
	// 	wrapper.find('#decrement-btn').simulate('click');
	// 	expect(wrapper.find('#counter-value').text()).toBe('-1');
	// });
});
