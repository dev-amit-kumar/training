import React, { useState } from 'react';
import Button from '../button';
import './counter.scss';

const App = () => {
	const [counter, setCounter] = useState(0);

	return (
		<div className="App">
			<span id="counter-value">{counter}</span>
			<Button
				cls="decrement-div"
				fn={() => setCounter((counter) => counter - 1)}
				id_name="decrement-btn"
				key="decrement"
				val="-"
			/>
			<Button
				cls="increment-div"
				fn={() => setCounter((counter) => counter + 1)}
				id_name="increment-btn"
				key="increment"
				val="+"
			/>
		</div>
	);
};

export default App;
