import React from 'react';
import { string, func } from 'prop-types';
import './button.scss';

const prop_types = {
	// cls is class name of div container
	cls: string,
	// fn is callback function of button
	fn: func,
	// id_name is the id of the button
	id_name: string,
	// val is the value of the button
	val: string,
};

const Button = ({ fn, id_name, val, cls }) => {
	return (
		<div className={cls}>
			<button id={id_name} onClick={fn}>
				{val}
			</button>
			<hr />
		</div>
	);
};

Button.propTypes = prop_types;

export default Button;
