import React, { useContext } from 'react';
import { FirstName, SecondName } from './Main';

function NameComponent() {
	const fname = useContext(FirstName);
	const lname = useContext(SecondName);
	return (
		<h1>
			My name is {fname} {lname}
		</h1>
	);
}

export default NameComponent;
