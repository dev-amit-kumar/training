import React from 'react';
import PropTypes from 'prop-types';
import NameComponent from './NameComponent';

function MiddleComponent({ person }) {
	return (
		<div>
			<h1>Profile Details of:{person} </h1>
			<NameComponent />
		</div>
	);
}

MiddleComponent.propTypes = {
	person: PropTypes.string.isRequired,
};

export default MiddleComponent;
