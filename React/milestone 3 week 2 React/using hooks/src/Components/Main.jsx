import React, { useState, useEffect } from 'react';
import MiddleComponent from './MiddleComponent';

export const FirstName = React.createContext();
export const SecondName = React.createContext();

function Main() {
	const [fname, setFname] = useState('');
	const [lname, setLname] = useState('');
	useEffect(() => {
		setFname('amit');
		setLname('kumar');
	}, [fname]);
	return (
		<FirstName.Provider value={fname}>
			<SecondName.Provider value={lname}>
				<MiddleComponent />
			</SecondName.Provider>
		</FirstName.Provider>
	);
}

export default Main;
