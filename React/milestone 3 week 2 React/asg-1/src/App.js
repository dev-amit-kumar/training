import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Index from './Components/Index';
import PageNotFound from './Components/PageNotFound';
import './style/index.scss';

function App() {
	return (
		<BrowserRouter>
			<Switch>
				<Route path="/" component={Index} />
				<Route component={PageNotFound} />
			</Switch>
		</BrowserRouter>
	);
}

export default App;
