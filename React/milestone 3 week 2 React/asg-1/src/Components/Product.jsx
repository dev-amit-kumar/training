import React from 'react';
import PropTypes from 'prop-types';

const Product = (props) => {
	return (
		<div className="product-detail">
			<img src={props.src} alt={props.alt} />
			<div className="product-body">
				<p>{props.name}</p>
				<p>
					Rs. {props.price} <del>Rs. {props.originalPrice}</del>
				</p>
				<button>Add to cart</button>
			</div>
		</div>
	);
};

Product.defaultProps = {
	alt: 'product',
};

Product.propTypes = {
	src: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	price: PropTypes.array.isRequired,
	originalPrice: PropTypes.number.isRequired,
};

export default Product;
