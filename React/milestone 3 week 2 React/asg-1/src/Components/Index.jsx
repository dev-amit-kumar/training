import React from 'react';
import Product from './Product';

function Index() {
	return (
		<div>
			<Product
				src="https://4.imimg.com/data4/AA/HC/MY-26596027/men-s-fancy-t-shirt-500x500.jpg"
				name="T-shirt"
				price="300"
				originalPrice="350"
			/>
		</div>
	);
}

export default Index;
