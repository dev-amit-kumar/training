import React from 'react';
import { Link } from 'react-router-dom';

function PageNotFound() {
	return (
		<div>
			<Link to="/">Sorry</Link>, Page not found
		</div>
	);
}

export default PageNotFound;
