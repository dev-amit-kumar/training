var hamberger_btn = document.querySelector('#hamberger');
var sidebar = document.querySelector('#sidebar');
var main = document.querySelector('section');
var user_avatar = document.querySelector('#user_avatar');
var sidebar_status = 'close';

window.addEventListener('resize', function () {
	if (window.innerWidth > 750) {
		sidebar.style.width = '200px';
		sidebar.style.display = 'block';
		sidebar_status = 'open';
	} else {
		sidebar_status = 'close';
		sidebar.style.display = 'none';
		sidebar.style.width = '0px';
	}
});

function closeSidebar() {
	if (sidebar_status == 'open' && window.innerWidth <= 750) {
		sidebar_status = 'close';
		sidebar.style.display = 'none';
		sidebar.style.width = '0px';
	}
}

function openSidebar() {
	if (sidebar_status == 'close' && window.innerWidth <= 750) {
		sidebar_status = 'open';
		sidebar.style.width = '200px';
		sidebar.style.display = 'block';
	}
}

main.addEventListener('click', closeSidebar);
user_avatar.addEventListener('click', closeSidebar);
hamberger_btn.addEventListener('click', openSidebar);

function showTabData(event, dataType) {
	console.log(dataType);
	var i, tab_content;

	tab_content = document.getElementsByClassName('tab_content');
	for (i = 0; i < tab_content.length; i++) {
		tab_content[i].classList.remove('active');
	}
	document.getElementById(dataType).classList.add('active');

	tab_button = document.getElementsByClassName('tab_button');
	for (i = 0; i < tab_button.length; i++) {
		tab_button[i].classList.remove('active');
	}
	event.currentTarget.className += ' active';
}
